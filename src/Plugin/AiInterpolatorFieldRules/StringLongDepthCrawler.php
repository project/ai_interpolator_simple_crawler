<?php

namespace Drupal\ai_interpolator_simple_crawler\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_simple_crawler\DepthCrawlerRule;

/**
 * The rules for a string_long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_simple_crawler_depth_string_long",
 *   title = @Translation("Simple Depth Crawler"),
 *   field_rule = "string_long"
 * )
 */
class StringLongDepthCrawler extends DepthCrawlerRule implements AiInterpolatorFieldRuleInterface {

  /**
   * {@inheritDoc}
   */
  public function getMode() {
    return 'string';
  }

}

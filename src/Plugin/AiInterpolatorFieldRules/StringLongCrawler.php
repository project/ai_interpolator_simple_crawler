<?php

namespace Drupal\ai_interpolator_simple_crawler\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator_simple_crawler\LongCrawlerBase;

/**
 * The rules for a string_long field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_simpler_crawler_string_long",
 *   title = @Translation("Simple Crawler"),
 *   field_rule = "string_long"
 * )
 */
class StringLongCrawler extends LongCrawlerBase {

  /**
   * {@inheritDoc}
   */
  public $title = 'Simple Crawler';

}

<?php

namespace Drupal\ai_interpolator_simple_crawler\Plugin\AiInterPolatorFieldRules;

use Drupal\ai_interpolator\PluginInterfaces\AiInterpolatorFieldRuleInterface;
use Drupal\ai_interpolator_simple_crawler\Crawler;
use Drupal\ai_interpolator_simple_crawler\CrawlerBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Utility\Token;
use Drupal\file\FileRepositoryInterface;
use ivan_boring\Readability\Configuration;
use ivan_boring\Readability\Readability;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * The rules for a image field.
 *
 * @AiInterpolatorFieldRule(
 *   id = "ai_interpolator_simpler_crawler_image",
 *   title = @Translation("Simple Image Crawler"),
 *   field_rule = "image",
 *   target = "file"
 * )
 */
class ImageCrawler extends CrawlerBase implements AiInterpolatorFieldRuleInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   */
  public EntityTypeManagerInterface $entityManager;

  /**
   * The current user.
   */
  public AccountProxyInterface $currentUser;

  /**
   * The File System interface.
   */
  public FileSystemInterface $fileSystem;

  /**
   * The File Repo.
   */
  public FileRepositoryInterface $fileRepo;

  /**
   * The token system to replace and generate paths.
   */
  public Token $token;

  /**
   * The crawler.
   */
  public Crawler $crawler;

  /**
   * Construct an image field.
   *
   * @param array $configuration
   *   Inherited configuration.
   * @param string $plugin_id
   *   Inherited plugin id.
   * @param mixed $plugin_definition
   *   Inherited plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityManager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The File system interface.
   * @param \Drupal\Core\Utility\Token $token
   *   The token system.
   * @param \Drupal\file\FileRepositoryInterface $fileRepo
   *   The File repo.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   * @param \Drupal\ai_interpolator_simple_crawler\Crawler $crawler
   *   The crawler requester.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityManager,
    FileSystemInterface $fileSystem,
    Token $token,
    FileRepositoryInterface $fileRepo,
    AccountProxyInterface $currentUser,
    Crawler $crawler,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $crawler);
    $this->entityManager = $entityManager;
    $this->fileSystem = $fileSystem;
    $this->fileRepo = $fileRepo;
    $this->currentUser = $currentUser;
    $this->token = $token;
    $this->crawler = $crawler;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('file_system'),
      $container->get('token'),
      $container->get('file.repository'),
      $container->get('current_user'),
      $container->get('ai_interpolator_simple_crawler.crawler'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public $title = 'Simple Image Crawler';

  /**
   * {@inheritDoc}
   */
  public function needsPrompt() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function advancedMode() {
    return FALSE;
  }

  /**
   * {@inheritDoc}
   */
  public function placeholderText() {
    return "";
  }

  /**
   * {@inheritDoc}
   */
  public function allowedInputs() {
    return ['link'];
  }

  /**
   * {@inheritDoc}
   */
  public function generate(ContentEntityInterface $entity, FieldDefinitionInterface $fieldDefinition, array $interpolatorConfig) {
    $readability = new Readability(new Configuration());
    $uris = $entity->get($interpolatorConfig['base_field'])->getValue();
    // Scrape.
    $values = [];
    foreach ($uris as $uri) {
      $rawHtml = $this->crawler->request($uri['uri'], $interpolatorConfig);

      $done = $readability->parse($rawHtml);
      $values[] = $done ? $readability->getImage() : '';
    }
    return $values;
  }

  /**
   * {@inheritDoc}
   */
  public function verifyValue(ContentEntityInterface $entity, $value, FieldDefinitionInterface $fieldDefinition) {
    // Should be an url.
    if (!filter_var($value, FILTER_VALIDATE_URL)) {
      return FALSE;
    }
    // Otherwise it is ok.
    return TRUE;
  }

  /**
   * {@inheritDoc}
   */
  public function storeValues(ContentEntityInterface $entity, array $values, FieldDefinitionInterface $fieldDefinition) {
    $config = $fieldDefinition->getConfig($entity->bundle())->getSettings();
    // Transform string to boolean.
    $fileEntities = [];

    // Successful counter, to only download as many as max.
    $successFul = 0;
    foreach ($values as $value) {
      // Save filename.
      $fileName = explode('?', basename($value))[0];
      // If no ending exists.
      if (!strstr($fileName, '.')) {
        $fileName .= '.jpg';
      }
      // Everything validated, then we prepare the file path to save to.
      $filePath = $this->token->replace($config['uri_scheme'] . '://' . rtrim($config['file_directory'], '/')) . '/' . $fileName;
      // Create file entity from string.
      $binary = file_get_contents($value);
      $file = $this->generateFileFromString($binary, $filePath);

      // If we can save, we attach it.
      if ($file) {
        // Get resolution.
        $resolution = getimagesize($file->uri->value);
        // Add to the entities list.
        $fileEntities[] = [
          'target_id' => $file->id(),
          'alt' => $config['default_image']['alt'] ?? '',
          'title' => $config['default_image']['title'] ?? '',
          'width' => $resolution[0],
          'height' => $resolution[1],
        ];

        $successFul++;
        // If we have enough images, give up.
        if ($successFul == $fieldDefinition->getFieldStorageDefinition()->getCardinality()) {
          break;
        }
      }
    }

    // Then set the value.
    $entity->set($fieldDefinition->getName(), $fileEntities);
  }

  /**
   * Generate a file entity.
   *
   * @param string $binary
   *   The source binary.
   * @param string $dest
   *   The destination.
   *
   * @return \Drupal\file\FileInterface|false
   *   The file or false on failure.
   */
  private function generateFileFromString(string $binary, string $dest) {
    $path = substr($dest, 0, -(strlen($dest) + 1));
    // Create directory if not existsing.
    $this->fileSystem->prepareDirectory($path, FileSystemInterface::CREATE_DIRECTORY);
    $file = $this->fileRepo->writeData($binary, $dest, FileSystemInterface::EXISTS_RENAME);
    // Check file entity.
    if ($file) {
      return $file;
    }
    return FALSE;
  }

}

<?php

namespace Drupal\ai_interpolator_simple_crawler;

use GuzzleHttp\Client;

/**
 * Guzzle Crawler API.
 */
class Crawler {

  /**
   * The http client.
   */
  protected Client $client;

  /**
   * Constructs a new Crawler object.
   *
   * @param \GuzzleHttp\Client $client
   *   Http client.
   */
  public function __construct(Client $client) {
    $this->client = $client;
  }

  /**
   * Crawl.
   *
   * @param string $url
   *   The url to visit.
   * @param array $parameters
   *   The parameters for the call. q have to be set.
   *
   * @return string
   *   The body response.
   */
  public function request($url, array $parameters) {
    $options['connect_timeout'] = $parameters['connect_timeout'] ?? 15;
    $options['read_timeout'] = $parameters['read_timeout'] ?? 15;

    // Don't fail on 404.
    $options['http_errors'] = FALSE;

    // Set custom headers.
    if (!empty($parameters['custom_headers'])) {
      $headers = $this->textareaToArray($parameters['custom_headers']);
      if (!empty($headers)) {
        $options['headers'] = $headers;
      }
    }

    // Set custom cookies.
    if (!empty($parameters['custom_cookies'])) {
      $cookies = $this->textareaToArray($parameters['custom_cookies'], '=');
      if (!empty($cookies)) {
        $options['headers']['Cookie'] = '';
        foreach ($cookies as $key => $val) {
          $options['headers']['Cookie'] .= "$key=$val; ";
        }
      }
    }

    // Set user agent.
    if (!empty($parameters['user_agent'])) {
      $options['headers']['user-agent'] = $parameters['user_agent'];
    }

    // Set basic auth.
    if (!empty($parameters['basic_auth_username']) && !empty($parameters['basic_auth_password'])) {
      $options['auth'] = [
        $parameters['basic_auth_username'],
        $parameters['basic_auth_password'],
      ];
    }

    // Send request.
    $response = $this->client->request('GET', $url, $options);
    if (!empty($parameters['rate_limit_wait'])) {
      usleep($parameters['rate_limit_wait']);
    }
    return $response->getBody();
  }

  /**
   * Convert textarea to array.
   *
   * @param string $text
   *   The text to convert.
   * @param string $separator
   *   The separator.
   *
   * @return array
   *   The array.
   */
  protected function textareaToArray($text, $separator = ":") {
    $lines = explode("\n", $text);
    $return = [];
    foreach ($lines as $line) {
      $line = explode($separator, $line);
      if (count($line) !== 2) {
        continue;
      }
      $return[trim($line[0])] = trim($line[1]);
    }
    return $return;
  }

}
